export interface Wedding {
    id: number;
    clientFname: string;
    clientLname: String;
    location: string;
    budget: number;
    created: Date;
    modified: Date;
    due: Date;
}