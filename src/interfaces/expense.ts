export interface Expense {
    id: number;
    reason: string;
    amount: number;
    receiptImg: string;
    created: Date,
    modified: Date;
    wID: number;
}