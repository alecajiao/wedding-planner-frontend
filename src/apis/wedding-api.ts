import axios from "axios";
import { Wedding } from "../interfaces/wedding-interface";

export class WeddingAPI {
    static async createWedding(wedding:any){
        try {
            return (await axios.post("http://34.133.53.120:3001/weddings", wedding)).data;
                  
        } catch (error) {
            console.log(error);
            return error;
        }
    }

    static async getWeddings(){
        try {
            return (await axios.get("http://34.133.53.120:3001/weddings")).data;            
        } catch (error) {
            console.log(error);
        }
    }

    static async getExpensesForWedding(id:number){
        try {
            return (await axios.get(`http://34.133.53.120:3001/weddings/${id}/expenses`)).data;
        } catch (error) {
            console.log(error);
        }
    }

    static async updateWedding(wedding:any){
        try {
            return (await axios.put(`http://34.133.53.120:3001/weddings/${wedding.id}`, wedding)).data;
        } catch (error) {
            console.log(error);
        }
    }

    static async deleteWedding(id:number){
        try {
            const result =  await axios.delete(`http://34.133.53.120:3001/weddings/${id}`);
            console.log(result);
            return result;
            
        } catch (error) {
            console.log(error);
        }
    }
}
