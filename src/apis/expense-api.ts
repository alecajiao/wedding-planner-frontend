import { RestoreOutlined } from "@material-ui/icons";
import axios from "axios";

export class ExpenseAPI{
    static async createExpense(expense:any){
        try {
            return (await axios.post(`http://34.133.53.120:3001/expenses`, expense)).data;  
        } catch (error) {
            console.log(error);
        }
    };

    static async getExpenses(){
        try {
            return (await axios.get("http://34.133.53.120:3001/expenses")).data;
        } catch (error) {
            console.log(error);
            
        }
    }

    static async updateExpense(expense:any){        
        try {
            return (await axios.put(`http://34.133.53.120:3001/expenses/${expense.id}`, expense)).data;
            
        } catch (error) {
            console.log(error);
        }
    };
    
    static async deleteExpense(id:any){   
        try { 
            const result = await axios.delete(`http://34.133.53.120:3001/expenses/${id}`);
            console.log(result);
            return result;
            
        } catch (error) {
            console.log(error);
        }
    };
}