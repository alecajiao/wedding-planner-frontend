import axios from "axios";

export class UserAPI{
    static async userLogin(userCredentials:any){
        console.log(userCredentials);

        try {
            //credentials expected format
            // {"email": "roxi@email.com", "password": "123456"}
            const response = await axios.patch('http://wedding-planner-authentication-service.wedding-planner-p1-cajiao.ue.r.appspot.com/users/login', userCredentials);
            console.log(response);
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            return response;
            
            
        } catch (error) {
            console.log(error);
        }
        
    };

    static async verifyUser(userEmail:any){
        try {
            console.log(userEmail);
            const response = await axios.get(`http://wedding-planner-authentication-service.wedding-planner-p1-cajiao.ue.r.appspot.com/users/${userEmail}/verify`);
            console.log(response);
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            return response;
        } catch (error) {
            console.log(error);
            
        }
    };
}