import axios from "axios";

export class MessageAPI{
    // I don't need this functionality on the frontend (maybe an admin dash feature)
    static async getAllMessages(){};

    static async getAllMessagesBySender(sender:any){
        try {
            console.log(sender);
            
            // const response:any = await axios.get(`https://wedding-planner-messaging-service-dot-wedding-planner-p1-cajiao.ue.r.appspot.com/messages?recipient=${recipient}`);
            // console.log(response);
            // console.log(response.data);
            return (await axios.get(`https://wedding-planner-messaging-service-dot-wedding-planner-p1-cajiao.ue.r.appspot.com/messages?sender=${sender}`)).data;
            
        } catch (error) {
            console.log(error);
            
        }
    };

    static async getAllMessagesByRecipient(recipient:any){
        try {
            console.log(recipient);
            
            // const response:any = await axios.get(`https://wedding-planner-messaging-service-dot-wedding-planner-p1-cajiao.ue.r.appspot.com/messages?recipient=${recipient}`);
            // console.log(response);
            // console.log(response.data);
            return (await axios.get(`https://wedding-planner-messaging-service-dot-wedding-planner-p1-cajiao.ue.r.appspot.com/messages?recipient=${recipient}`)).data;
            
        } catch (error) {
            console.log(error);
            
        }
    };

    static async getALlMessagesBySenderAndRecipient(sender:any, recipient:any){
        try {
            console.log(sender);
            console.log(recipient);

            const response = await axios.get(`https://wedding-planner-messaging-service-dot-wedding-planner-p1-cajiao.ue.r.appspot.com/messages?sender=${sender}&recipient=${recipient}`);
            console.log(response);
            console.log(response.data);
            return response;
            
        } catch (error) {
            console.log(error);
            
        }
    };

    static async createMessage(message:any){
        try {
            console.log(message);
            const response = await axios.post('https://wedding-planner-messaging-service-dot-wedding-planner-p1-cajiao.ue.r.appspot.com/messages', message);

            console.log(response.data);
            return response;

             
        } catch (error) {
            console.log(error);
            
        }
    };
}