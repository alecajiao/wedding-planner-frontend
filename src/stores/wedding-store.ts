import { createStore, Reducer } from "redux";


//Wedding Planner internal state
export interface WeddingPlannerState{
    wedding:any,
    expense:any,
    user:any,
    message:any,
    weddings:[],
    messages:[]
}

const reducer:Reducer = (currentWeddingPlanerState:WeddingPlannerState = {wedding:'', expense:'', user:'', message: '', weddings: [], messages: []}, message:any) => {
    switch (message.type) {
        case "saveWedding":
            const newWedding:WeddingPlannerState = {
                wedding: message.wedding,
                expense: currentWeddingPlanerState.expense,
                user: currentWeddingPlanerState.user,
                message: currentWeddingPlanerState.message,
                weddings: currentWeddingPlanerState.weddings,
                messages: currentWeddingPlanerState.messages
            }
            return newWedding;
        case "saveExpense":
            const newExpense:WeddingPlannerState = {
                expense: message.expense,
                wedding: currentWeddingPlanerState.wedding,
                user: currentWeddingPlanerState.user,
                message: currentWeddingPlanerState.message,
                weddings: currentWeddingPlanerState.weddings,
                messages: currentWeddingPlanerState.messages
            }
            return newExpense;
        case "saveUser":
            const newUser:WeddingPlannerState = {
                expense: currentWeddingPlanerState.expense,
                wedding: currentWeddingPlanerState.wedding,
                user: message.user,
                message: currentWeddingPlanerState.message,
                weddings: currentWeddingPlanerState.weddings,
                messages: currentWeddingPlanerState.messages
            }
            return newUser;
        case "saveMessage":
            const newMessage:WeddingPlannerState = {
                expense: currentWeddingPlanerState.expense,
                wedding: currentWeddingPlanerState.wedding,
                user: currentWeddingPlanerState.user,
                message: message.message,
                weddings: currentWeddingPlanerState.weddings,
                messages: currentWeddingPlanerState.messages
            }
            return newMessage;
        case "saveWeddings":
            const newWeddings:WeddingPlannerState = {
                expense: currentWeddingPlanerState.expense,
                wedding: currentWeddingPlanerState.wedding,
                user: currentWeddingPlanerState.user,
                message: currentWeddingPlanerState.message,
                weddings: message.weddings,
                messages: currentWeddingPlanerState.messages
            }
            return newWeddings;
        case "saveMessages":
            const newMessages:WeddingPlannerState = {
                expense: currentWeddingPlanerState.expense,
                wedding: currentWeddingPlanerState.wedding,
                user: currentWeddingPlanerState.user,
                message: currentWeddingPlanerState.message,
                weddings: currentWeddingPlanerState.weddings,
                messages: message.messages
            }
            return newMessages;
        default:
            return currentWeddingPlanerState;
    }
};

const store = createStore(reducer);

export default store;