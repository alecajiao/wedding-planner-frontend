
import { CardContent, Typography } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  // Credits
  //https://stackoverflow.com/questions/50252463/material-ui-next-setting-image-size-to-fit-a-container
  card: {
    // width: '100%',
    // height: '100%',
    // margin: 'auto'
  },
  media: {
    height: '100%',
    width: '100%',
    objectFit: 'contain'
  }
});

export default function ViewReceiptCard(props:any){
    const classes = useStyles();

    console.log(props.receiptImg);
    
    function ReceiptCard(){
      if(props.receiptImg){
        return(
          <Card className={classes.card}>
            <CardMedia
              className={classes.media}
              component="img"
              image={props.receiptImg}
              alt=''
            />
          </Card>
        );
      };

      return(
        <Card className={classes.card}>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            No Receipt Available
          </Typography>
          <Typography variant="body2">
            You can add a receipt by clicking on "EDIT RECEIPT" and uploading the receipt.
          </Typography>
        </CardContent>
      </Card>
      );
    }

    return(
        <>
        <ReceiptCard/>
        </>
    );
}