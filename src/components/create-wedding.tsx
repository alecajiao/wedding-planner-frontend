import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useRef } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import { WeddingAPI } from '../apis/wedding-api';
import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
        minWidth: 275,
      },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    title: {
        // color: 'rgba(0, 0, 0, 0.54)',
        color:'#424242',
      },
  }),
);

export default function CreateWedding(){
    const classes = useStyles();
    const [openSuccess, setOpenSuccess] = React.useState(false);

    // Add wedding form input values
    const fnameInput = useRef<any>(null);
    const lnameInput = useRef<any>(null);
    const budgetInput = useRef<any>(null);
    const locationInput = useRef<any>(null);
    const dueInput = useRef<any>(null);

    function clearForm(){
        fnameInput.current.value = null; 
        lnameInput.current.value = null;
        budgetInput.current.value = null;
        locationInput.current.value = null;
        dueInput.current.value = null;
    }

    const handleClickSuccess = () => {
        setOpenSuccess(true);
    };

    const handleCloseSuccess = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        
        setOpenSuccess(false);
    };


    const createWedding = async() => {
    const wedding = {
        client_fname: fnameInput.current.value,
        client_lname: lnameInput.current.value,
        budget: budgetInput.current.value,
        location: locationInput.current.value,
        due: dueInput.current.value,
        }

        console.log(wedding);  
        const createdWedding = await WeddingAPI.createWedding(wedding);
        console.log(createdWedding);
        clearForm(); 
        handleClickSuccess();
    };

    function WeddingCreatedAlert() {
        return (
        <>
            <Snackbar open={openSuccess} autoHideDuration={6000} onClose={handleCloseSuccess}>
            <Alert onClose={handleCloseSuccess} severity="success">
                Wedding has been created!
            </Alert>
            </Snackbar>
        </>
        );
    }


    function CreateWeddingForm(){
        return(
        <>
        <Grid item xs={12}>
        <Card className={classes.root} variant="outlined" elevation={1}>
        <CardContent>
            <TextField
            required
            margin="dense"
            id="fname"
            label="client first name"
            type="input"
            inputRef={fnameInput}
            // placeholder={currentWeddingState.wedding.client_fname}
            fullWidth
            />
            <TextField
            required
            margin="dense"
            id="lname"
            label="client last name"
            type="text"
            inputRef={lnameInput}
            // placeholder={currentWeddingState.wedding.client_lname}
            fullWidth
            />
            <TextField
            required
            margin="dense"
            id="budget"
            label="budget"
            type="currency"
            inputRef={budgetInput}
            fullWidth
            />
            <TextField
            required
            margin="dense"
            id="location"
            label="location"
            type="text"
            inputRef={locationInput}
            // placeholder={currentWeddingState.wedding.location}
            fullWidth
            />
            <TextField
            required
            margin="dense"
            id="due"
            // label="due"
            type="date"
            inputRef={dueInput}
            // placeholder={(new Date(currentWeddingState.wedding.due)).toDateString()}
            fullWidth
            />
            {/* <Button onClick={clear} color="primary">
            clear
            </Button> */}
        </CardContent>
        <CardActions>
            <Button size="small" onClick={createWedding}>create</Button>
        </CardActions>
        </Card>
    </Grid>
    </>
    );
    }

    return(
    <>
    <h1 className={classes.title}>Create Wedding</h1>
    <CreateWeddingForm/>
    <WeddingCreatedAlert/>
    </>);
}
