import Grid from '@material-ui/core/Grid';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import CreateWedding from './create-wedding';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);

export default function DisplayCreateWedding(){
    const classes = useStyles();

    return(
    <main className={classes.content}>
        <Toolbar />
        <CreateWedding/>
        
    </main>);
}