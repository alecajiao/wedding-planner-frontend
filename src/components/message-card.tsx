import { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { WeddingAPI } from '../apis/wedding-api';
import Grid from '@material-ui/core/Grid';
import CardActionArea from '@material-ui/core/CardActionArea';
import { Link } from 'react-router-dom';
import { MessageAPI } from '../apis/message-api';
import { useDispatch, useSelector } from "react-redux"


const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
});

const getMessages = async (recipient:any) => {
    return await MessageAPI.getAllMessagesByRecipient(recipient); 
}

export default function MessageCard() {
  const dispatch = useDispatch();
  const currentWeddingState = useSelector((state:any)=>state); 
  const classes = useStyles();
  const [messages, setMessages] = useState([] as any[]);

  useEffect(() => {
    console.log(currentWeddingState.user.email);
    getMessages(currentWeddingState.user.email).then((data:any ) => setMessages(data || [] as any[]));      
  }, []);

  const storeCurrentMessage = (m:any)=>{
    console.log(m);
    dispatch({type:"saveMessage",message: m});  
  };

  // Credits:
  // Reversing the array
  // https://stackoverflow.com/questions/36415904/is-there-a-way-to-use-map-on-an-array-in-reverse-order-with-javascript/52323834
  const messageCards = messages.slice(0).reverse().map(m => {
      return(<Grid item xs={12}>
        <Link to="/message">
        <Card className={classes.root} variant="outlined" elevation={3} onClick={()=>{storeCurrentMessage(m)}}>
          <CardActionArea>
            <CardContent>
              <Typography variant="h5" component="h2">
                From: {m.sender}
              </Typography>
              <Typography variant="body2" component="p">
                To: {m.recipient}
              </Typography>
              <Typography variant="body2" component="p">
                {m.note}
              </Typography>
              <Typography variant="body2" component="p" color="textSecondary">
                {(new Date(m.created)).toDateString()}
              </Typography>
            </CardContent>
          {/* <CardActions>
            <Button size="small">Learn More</Button>
          </CardActions> */}
          </CardActionArea>
        </Card>
        </Link> 
      </Grid>)
  }
  );
    
  return (<Grid
    container
    direction="row"
    justifyContent="flex-start"
    alignItems="flex-start"
    spacing={3}
  >
    {/* <h1>Messages</h1> */}
    {messageCards}
  </Grid>)
}