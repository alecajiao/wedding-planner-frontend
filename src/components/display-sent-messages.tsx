import { createStyles, Theme, makeStyles, alpha } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import MessageCard from './message-card';
import SentMessageCard from './sent-message-card';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);

export default function DisplaySentMessages() {
  const classes = useStyles();

  return (<main className={classes.content}>
    <Toolbar />
      <SentMessageCard/>
  </main>);
}