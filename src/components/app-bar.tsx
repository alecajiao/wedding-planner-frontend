import { createStyles, Theme, makeStyles, alpha } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import SearchIcon from '@material-ui/icons/Search';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import IconButton from '@material-ui/core/IconButton';
import InputBase from '@material-ui/core/InputBase';
import {
  Link
} from "react-router-dom";
import Button from '@material-ui/core/Button';
import React, { useRef } from 'react';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import axios from 'axios';
import { useDispatch, useSelector } from "react-redux"
import { UserAPI } from '../apis/user-api';
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';
import { MessageAPI } from '../apis/message-api';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    title: {
        flexGrow: 1,
        display: 'none',
        [theme.breakpoints.up('sm')]: {
          display: 'block',
        },
      },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: alpha(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
      },
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(1),
        width: 'auto',
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        width: '12ch',
        '&:focus': {
          width: '20ch',
        },
      },
    },
    menuButton: {
      marginRight: theme.spacing(2),
    }
  }),
);

export default function AppBarNav() {
    const classes = useStyles();
    const currentWeddingState = useSelector((state:any)=>state); 
    const dispatch = useDispatch();
    const [open, setOpen] = React.useState(false);
    const [openError, setOpenError] = React.useState(false);

    // Login form input values
    const emailInput = useRef<any>(null);
    const passwordInput = useRef<any>(null);

    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };

    const handleCloseError = (event?: React.SyntheticEvent, reason?: string) => {
      if (reason === 'clickaway') {
          return;
      }
      
      setOpenError(false);
  };

  const handleClickError = () => {
    setOpenError(true);
};

    const login = async () => {
      const userCredentials = {
        email: emailInput.current.value,
        password: passwordInput.current.value
      }

      console.log(userCredentials);
    
      try {
        const response:any = await UserAPI.userLogin(userCredentials);

        const user = {
          email: emailInput.current.value,
          password: passwordInput.current.value,
          fname: response.data.fname,
          lname: response.data.lname
        }
        // const user:any = response.data;
        console.log(user);
        console.log(response.data);
        dispatch({type:"saveUser", user: user }); 
        console.log(currentWeddingState);
        handleClose();
      } catch (error) {        
        console.log(error);
        handleClickError();
        //then tell user they credentials are not valid 
        //maybe make the form red
        
      }

      try {
        const messages = await MessageAPI.getAllMessagesByRecipient(userCredentials.email);
        console.log(messages);
        dispatch({type:"saveMessages", messages: messages }); 
        console.log(currentWeddingState);
        
        
      } catch (error) {
        console.log(error);
        
      }

      console.log(currentWeddingState);

    };

    const logout = () => {
      // currentWeddingState.user = '';
      console.log(currentWeddingState);
      dispatch({type: 'saveUser', user: ''});
      console.log(currentWeddingState);
      
    };

    function NavBar(){
      return(
      <>  
        <AppBar position="fixed" className={classes.appBar}>
           <Toolbar>         
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="open drawer"
            >
              <Link to="/">
              <CalendarTodayIcon></CalendarTodayIcon>
              </Link>
            </IconButton>
            <Typography className={classes.title} variant="h6" noWrap>
              Wedding Planner App
            </Typography>
            {/* <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                inputProps={{ 'aria-label': 'search' }}
              />
            </div> */}
            <LoginOption/>
            {/* <Link to="/login"><Button color="inherit" onClick={handleClickOpen}>Login</Button></Link> */}
          </Toolbar>
        </AppBar>
      </>);
    }

    function Login(){
      return(
      <>
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Login</DialogTitle>
        <DialogContent>
          {/* <DialogContentText>
            To subscribe to this website, please enter your email address here. We will send updates
            occasionally.
          </DialogContentText> */}
          <TextField
            error={currentWeddingState.user === null ? true : false}
            margin="dense"
            id="name"
            label="Email Address"
            type="email"
            inputRef={emailInput}
            fullWidth
          />
            {/* <TextField
              error
              id="standard-error-helper-text"
              // label="Error"
              defaultValue="Hello World"
              helperText="Incorrect entry."
              variant="standard"
            /> */}

          <TextField
            error={currentWeddingState.user === null ? true : false}
            margin="dense"
            id="password"
            label="password"
            type="password"
            inputRef={passwordInput}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
        {/* <Link to="/all-weddings"> */}
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          {/* </Link> */}
          <Link to="/">
          <Button onClick={login} color="primary">
            Submit
          </Button>
          </Link>
        </DialogActions>
      </Dialog>
      </>);
    }

    function LoginOption(){
      if(currentWeddingState.user){
        return(
          <>
          <Link to="/login"><Button color="inherit" onClick={logout}>Logout</Button></Link>
          <Button color="inherit">{currentWeddingState.user.fname}</Button>
          </>
        );
      }

      return(
        <>
        <Link to="/login"><Button color="inherit" onClick={handleClickOpen}>Login</Button></Link>
        </>
      );
    }

    function LoginFailedAlert() {
      return (
      <>
          <Snackbar open={openError} autoHideDuration={6000} onClose={handleCloseError}>
          <Alert onClose={handleCloseError} severity="error">
              Invalid Credentials
          </Alert>
          </Snackbar>
      </>
      );
  }
  
    return (
    <>
    <NavBar/>
    <Login/>
    <LoginFailedAlert/>
    </>);
  }