import { createStyles, Theme, makeStyles, alpha } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import {
  Route} from "react-router-dom";
import SideMenu from './side-menu';
import AppBarNav from './app-bar';
import DisplayWeddings from './display-weddings';
import DisplayWeddingDetails from './display-wedding-details';
import TextField from '@material-ui/core/TextField';
import DisplayCreateWedding from './display-create-wedding';
import DisplayMessages from './display-messages';
import DisplayCreateMessage from './display-create-message';
import DisplaySentMessages from './display-sent-messages';

// const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
  }),
);

export default function Main() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBarNav></AppBarNav>
      <SideMenu></SideMenu>

      <Route path="/wedding">
      <DisplayWeddingDetails></DisplayWeddingDetails>
      </Route>

      <Route path="/all-weddings">
        <DisplayWeddings></DisplayWeddings>
      </Route>

      <Route path="/create-wedding">
        <DisplayCreateWedding></DisplayCreateWedding>
      </Route>

      <Route path="/messages">
        <DisplayMessages/>
      </Route>

      <Route path="/create-message">
        <DisplayCreateMessage/>
      </Route>

      <Route path="/messages-sent">
        <DisplaySentMessages/>
      </Route>
      
    </div>
  );
}
