import { createStyles, Theme, makeStyles, alpha } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import MessageCard from './message-card';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);

export default function DisplayMessages() {
  const classes = useStyles();

  return (<main className={classes.content}>
    <Toolbar />
      <MessageCard></MessageCard>
  </main>);
}