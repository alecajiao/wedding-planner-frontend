import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import AllInboxIcon from '@material-ui/icons/AllInbox';
import ArchiveIcon from '@material-ui/icons/Archive';
import InboxIcon from '@material-ui/icons/Inbox';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';


const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerContainer: {
      overflow: 'auto',
    },
  }),
);

function setIcon(i: any){
  switch (i) {
    case 0:
      return <Link className="menuIcon" to="/create-wedding"><AddCircleIcon /></Link>;
    case 1:
      return <Link className="menuIcon" to="/all-weddings"><ArchiveIcon /></Link>;
    case 2:
      return <Link className="menuIcon" to="/create-message"><MailIcon/></Link>;
    case 3:
      return <Link className="messagesIcon"to="/messages"><InboxIcon /></Link>;
    case 4:
      return <Link className="messagesIcon"to="/messages-sent"><SendIcon /></Link>;
    default:
      break;
  }
}

export default function SideMenu() {
  const classes = useStyles();
  const currentWeddingState = useSelector((state:any)=>state); 

  function WeddingSideMenuList(){
    return(
    <>
    <List>
    {['Create Wedding', 'All Weddings', 'Send Message', 'Messages', 'Sent Messages'].map((text, index) => (
      <ListItem button key={text}>
        <ListItemIcon>{setIcon(index)}</ListItemIcon>
        <ListItemText primary={text} />
      </ListItem>
    ))}
    </List>
    </>);
  }

  function MessagesSideMenuList(){
    return(
      <>
      <List>
        {['All mail', 'Trash', 'Spam'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
      </>
    );
  }

  function IsLoggedInShowMenu(){
    console.log(currentWeddingState);
    
    if(currentWeddingState.user){
      return(
        <>
        <WeddingSideMenuList/>
        {/* <Divider />
        <MessagesSideMenuList/> */}
        </>
      )
    }
    return (<></>);
  }

  return (<Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar />
        <div className={classes.drawerContainer}>
          {/* <WeddingSideMenuList/>
          <Divider />
          <MessagesSideMenuList/> */}
          <IsLoggedInShowMenu/>
        </div>
      </Drawer>
  );
}
