import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useRef } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import { WeddingAPI } from '../apis/wedding-api';
import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import { useSelector } from 'react-redux';
import { MessageAPI } from '../apis/message-api';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
        minWidth: 275,
      },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    title: {
        // color: 'rgba(0, 0, 0, 0.54)',
        color:'#424242',
      },
  }),
);

export default function CreateMessage(){
    const classes = useStyles();
    const currentWeddingState = useSelector((state:any)=>state); 
    const [openSuccess, setOpenSuccess] = React.useState(false);
    const [openError, setOpenError] = React.useState(false);

    // Add message form input values
    const recipientInput = useRef<any>(null);
    const noteInput = useRef<any>(null);
    // const budgetInput = useRef<any>(null);
    // const locationInput = useRef<any>(null);
    // const dueInput = useRef<any>(null);

    function clearForm(){
        recipientInput.current.value = null; 
        noteInput.current.value = null;
        // budgetInput.current.value = null;
        // locationInput.current.value = null;
        // dueInput.current.value = null;
    }

    const handleClickSuccess = () => {
        setOpenSuccess(true);
    };

    const handleCloseSuccess = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        
        setOpenSuccess(false);
    };

    const handleCloseError = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        
        setOpenError(false);
    };
  
    const handleClickError = () => {
      setOpenError(true);
  };


    const createMessage = async() => {
    const message = {
        recipient: recipientInput.current.value,
        sender: currentWeddingState.user.email,
        note: noteInput.current.value 
        }
        console.log(message); 

        try {
            const createdMessage = await MessageAPI.createMessage(message);
            console.log(createdMessage);

            if(createdMessage){
                clearForm(); 
                handleClickSuccess();
            }else{
                clearForm();
                handleClickError()
            }

            
        } catch (error) {
            console.log(error); 
            
        }
    };

    function MessageSentAlert() {
        return (
        <>
            <Snackbar open={openSuccess} autoHideDuration={6000} onClose={handleCloseSuccess}>
            <Alert onClose={handleCloseSuccess} severity="success">
                Message sent!
            </Alert>
            </Snackbar>
        </>
        );
    }

    function UnableToVerifyAccountsAlert() {
        return (
        <>
            <Snackbar open={openError} autoHideDuration={6000} onClose={handleCloseError}>
            <Alert onClose={handleCloseError} severity="error">
              Unable to verify accounts
            </Alert>
            </Snackbar>
        </>
        );
    }


    function SendMessageForm(){
        return(
        <>
        <Grid item xs={12}>
        <Card className={classes.root} variant="outlined" elevation={1}>
        <CardContent>
            <TextField
            required
            margin="dense"
            id="recipient"
            label="to"
            type="input"
            inputRef={recipientInput}
            fullWidth
            />
            <TextField
            required
            margin="dense"
            id="note"
            label="note"
            type="text"
            inputRef={noteInput}
            fullWidth
            />
            {/* <TextField
            required
            margin="dense"
            id="budget"
            label="budget"
            type="currency"
            inputRef={budgetInput}
            fullWidth
            />
            <TextField
            required
            margin="dense"
            id="location"
            label="location"
            type="text"
            inputRef={locationInput}
            fullWidth
            />
            <TextField
            required
            margin="dense"
            id="due"
            type="date"
            inputRef={dueInput}
            fullWidth
            /> */}
            {/* <Button onClick={clear} color="primary">
            clear
            </Button> */}
        </CardContent>
        <CardActions>
            <Button size="small" onClick={createMessage}>send</Button>
        </CardActions>
        </Card>
    </Grid>
    </>
    );
    }

    return(
    <>
    <h1 className={classes.title}>Send Message</h1>
    <SendMessageForm/>
    <MessageSentAlert/>
    <UnableToVerifyAccountsAlert/>
    </>);
}
