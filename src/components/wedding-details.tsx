import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { useSelector } from "react-redux";
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { WeddingAPI } from '../apis/wedding-api';
import { useEffect, useRef, useState } from 'react';
import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CardActions from '@material-ui/core/CardActions';
import { useDispatch } from 'react-redux';
import { ExpenseAPI } from '../apis/expense-api';
// import { Link } from '@material-ui/core';
import { Link } from 'react-router-dom';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import axios from 'axios';
import ViewReceiptCard from './view-receipt-card';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  title: {
    // color: 'rgba(0, 0, 0, 0.54)',
    color:'#424242',
  },
  name: {
    color:'#424242',
  },
  dialog: {
    // fullWidth: true,
    // maxWidth: 'lg',
    // maxHeight: '100%',
    // fullScreen: true
    // height: '100%'
  }
});

export default function WeddingDetails() {
  const currentWeddingState = useSelector((state:any)=>state); 
  console.log(currentWeddingState);
  
  const dispatch = useDispatch(); 
  const classes = useStyles();
  const [expenses, setExpenses] = useState([] as any[]);
  const [openEditWeddingDialog, setOpenEditWeddingDialog] = React.useState(false);
  const [openEditExpenseDialog, setOpenEditExpenseDialog] = React.useState(false);
  const [openAddExpense, setOpenAddExpense] = React.useState(false);
  const [openDeleteWeddingConfirmation, setOpenDeleteWeddingConfirmation] = React.useState(false);
  const [openSuccessAlert, setOpenSuccessAlert] = React.useState(false);
  const [openViewReceipt, setOpenViewReceipt] = React.useState(false);

  // Edit wedding form input values
  const fnameInput = useRef<any>(null);
  const lnameInput = useRef<any>(null);
  const budgetInput = useRef<any>(null);
  const locationInput = useRef<any>(null);
  const dueInput = useRef<any>(null);

  // Edit expense form input values
  const amountInput = useRef<any>(null);
  const reasonInput = useRef<any>(null);
  let receiptImg:string = '';

  useEffect(()=>{
    WeddingAPI.getExpensesForWedding(currentWeddingState.wedding.id).then((data:any) => {setExpenses(data || [] as any[])});   
  }, [currentWeddingState]);

  const handleClickSuccess = () => {
    setOpenSuccessAlert(true);
  };
  const handleOpenViewReceipt = () => {
    setOpenViewReceipt(true);
  };

  const handleCloseViewReceipt = () => {
    setOpenViewReceipt(false);
  };

  const handleCloseSuccessAlert = (event?: React.SyntheticEvent, reason?: string) => {
      if (reason === 'clickaway') {
          return;
      }
      
      setOpenSuccessAlert(false);
  };

  const handleClickOpenDeleteWeddingConfirmation = () => {
    setOpenDeleteWeddingConfirmation(true);
  };

  const handleCloseDeleteWeddingConfirmation = () => {
    setOpenDeleteWeddingConfirmation(false);
  };

  const handleClickOpenEditWedding = () => {
    setOpenEditWeddingDialog(true);
  };

  const handleCloseEditWeddingDialog = () => {    
    setOpenEditWeddingDialog(false);
  };

  const handleClickOpenExpense = () => {
    setOpenEditExpenseDialog(true);
  };

  const handleCloseEditExpenseDialog = () => {    
    setOpenEditExpenseDialog(false);
  };

  const handleClickOpenAddExpense = () => {
    setOpenAddExpense(true);
  };

  const handleCloseAddExpense = () => {    
    setOpenAddExpense(false);
  };

  const storeCurrentExpense = (expense:any) => {
    dispatch({type: "saveExpense",expense: expense });
  };

  const editWedding = async () => {
    const editedWedding = {
      client_fname: fnameInput.current.value !== "" ? fnameInput.current.value : currentWeddingState.wedding.client_fname,
      client_lname: lnameInput.current.value !== "" ? lnameInput.current.value : currentWeddingState.wedding.client_lname,
      budget: budgetInput.current.value !== "" ? budgetInput.current.value : currentWeddingState.wedding.budget,
      location: locationInput.current.value !== "" ? locationInput.current.value : currentWeddingState.wedding.location,
      due: dueInput.current.value !== "" ? dueInput.current.value : currentWeddingState.wedding.due,
      id: currentWeddingState.wedding.id
    }

    const updateWedding = await WeddingAPI.updateWedding(editedWedding);

    dispatch({type:"saveWedding",wedding: updateWedding });
    
    setOpenEditWeddingDialog(false);
    handleClickSuccess();

  };
  
  const deleteWedding = async()=>{
    console.log(currentWeddingState);
    
    const result = await WeddingAPI.deleteWedding(currentWeddingState.wedding.id);
    console.log(result);

    currentWeddingState.wedding = '';
    currentWeddingState.expenses = '';

    console.log(currentWeddingState);
    

    handleCloseDeleteWeddingConfirmation();
    handleClickSuccess();
    
  };

  const editExpense = async () => {
    const editedExpense = {
      id: currentWeddingState.expense.id,
      reason: reasonInput.current.value !== "" ? reasonInput.current.value : currentWeddingState.expense.reason,
      amount: amountInput.current.value !== "" ? amountInput.current.value : currentWeddingState.expense.amount,
      receiptImg: receiptImg !== '' ? receiptImg : currentWeddingState.expense.receiptImg,
      modified: currentWeddingState.expense.modified,
      wID: currentWeddingState.wedding.id,
    }

    console.log(editedExpense);
    

    const updatedExpense =  await ExpenseAPI.updateExpense(editedExpense);
    console.log(updatedExpense);

    dispatch({type:"saveExpense",expense: updatedExpense });
    
    const updatedExpenses:any = await WeddingAPI.getExpensesForWedding(currentWeddingState.wedding.id);
    
    setExpenses(updatedExpenses);

    setOpenEditExpenseDialog(false);
    handleClickSuccess();
  };

  const deleteExpense = async (expense:any) => {
    storeCurrentExpense(expense);
    await ExpenseAPI.deleteExpense(currentWeddingState.expense.id);
    const updatedExpenses:any = await WeddingAPI.getExpensesForWedding(currentWeddingState.wedding.id);
    setExpenses(updatedExpenses);
    handleClickSuccess();
  };

  // credits:
  // https://stackoverflow.com/questions/40589302/how-to-enable-file-upload-on-reacts-material-ui-simple-input
  const getFile = async ({target}:any) => {
    const fileReader = new FileReader();
    const fileName = target.files[0].name;
    // console.log(target);
    // console.log(target.files[0]);
    // console.log(target.files[0].name)

    fileReader.readAsDataURL(target.files[0]);
    fileReader.onload = async (e) => {
      // console.log(e.target?.result);
      const data:any = e.target?.result;
      const i = (data || '').indexOf(',');
      const img64 = data.substring(i + 1);
      // console.log(img64);
      const imgLink = await axios.post('https://us-central1-wedding-planner-p1-cajiao.cloudfunctions.net/wedding-planner-receipt-upload', {imgName: fileName, img64: img64});
      console.log(imgLink.data.photoLink);
      receiptImg = imgLink.data.photoLink;
      console.log(receiptImg);
    };
    
  }

  const addExpense = async () => {
    const newExpense = {
      reason:reasonInput.current.value,
      amount: amountInput.current.value,
      receiptImg: receiptImg,
      wID: currentWeddingState.wedding.id
    };

    console.log(newExpense);
    
    const addedExpense = await ExpenseAPI.createExpense(newExpense);
    console.log(addedExpense);
    
    const updatedExpenses:any = await WeddingAPI.getExpensesForWedding(currentWeddingState.wedding.id);
    setExpenses(updatedExpenses);
    handleCloseAddExpense();
    handleClickSuccess();
  }

  const expenseCards = expenses.map((e, i )=> {
    return (
      <Grid item xs={12} key={e}>
        <Card className={classes.root} variant="outlined" elevation={1} onClick={()=>{storeCurrentExpense(e)}}>
            <CardContent>
            <Typography variant="body2" component="p">
                {new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(e.amount)}
              </Typography>
              <Typography variant="body2" component="p" color="textSecondary">
                {e.reason}
              </Typography>
            </CardContent>
            <CardActions>
            <Button size="small" onClick={handleClickOpenExpense}>edit expense</Button>
            <Button size="small" onClick={handleOpenViewReceipt}>view receipt</Button>
            <Button size="small" onClick={(e)=>{deleteExpense(e)}}>delete</Button>
          </CardActions>
        </Card>
      </Grid>
    )
  });

  function WeddingCard(){
    return(
    <>
      <Grid item xs={12}>
        {/* <Link to="/wedding"> */}
        <Card className={classes.root} variant="outlined" elevation={1}>
            <CardContent>
              <Typography  className={classes.name}variant="h5" component="h2">
                {`${currentWeddingState.wedding.client_fname} ${currentWeddingState.wedding.client_lname}`}
              </Typography>
              <Typography variant="body2" component="p">
                {new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(currentWeddingState.wedding.budget)}
              </Typography>
              <Typography variant="body2" component="p" color="textSecondary">
                {currentWeddingState.wedding.location}
              </Typography>
              <Typography variant="body2" component="p" color="textSecondary">
                {(new Date(currentWeddingState.wedding.due)).toDateString()}
              </Typography>
            </CardContent>
          <CardActions>
            <Button size="small" onClick={handleClickOpenAddExpense}>add expense</Button>
            <Button size="small" onClick={handleClickOpenEditWedding}>edit wedding</Button>
            <Button size="small" onClick={handleClickOpenDeleteWeddingConfirmation}>delete</Button>
          </CardActions>
        </Card>
        {/* </Link>  */}
      </Grid>
    </>
    );
  }

  function WeddingContainer(){
    return(
      <>
      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignItems="flex-start"
        spacing={2}
      >
      <WeddingCard/>
      </Grid>
      </>
    )
  }

  function EditWeddingDialog(){
    return (
    <>
        <Dialog open={openEditWeddingDialog} onClose={handleCloseEditWeddingDialog} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Edit Wedding</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Make changes to the wedding details and then press the submit button to commit the changes.
          </DialogContentText>
          <TextField
            margin="dense"
            id="fname"
            label="client first name"
            type="input"
            inputRef={fnameInput}
            placeholder={currentWeddingState.wedding.client_fname}
            fullWidth
          />
          <TextField
            margin="dense"
            id="lname"
            label="client last name"
            type="text"
            inputRef={lnameInput}
            placeholder={currentWeddingState.wedding.client_lname}
            fullWidth
          />
          <TextField
            margin="dense"
            id="budget"
            label="budget"
            type="currency"
            inputRef={budgetInput}
            placeholder={new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(currentWeddingState.wedding.budget)}
            fullWidth
          />
          <TextField
            margin="dense"
            id="location"
            label="location"
            type="text"
            inputRef={locationInput}
            placeholder={currentWeddingState.wedding.location}
            fullWidth
          />
          <TextField
            margin="dense"
            id="due"
            // label="due"
            type="date"
            inputRef={dueInput}
            placeholder={(new Date(currentWeddingState.wedding.due)).toDateString()}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseEditWeddingDialog} color="primary">
            Cancel
          </Button>
          <Button onClick={editWedding} color="primary">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </>
    )
  }

  function DeleteWeddingConfirmation(){
    return(
    <>
     <Dialog
        open={openDeleteWeddingConfirmation}
        onClose={handleCloseDeleteWeddingConfirmation}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Are you sure you want to delete this wedding?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
          This action is permanent.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDeleteWeddingConfirmation} color="primary">
            cancel
          </Button>
          <Link to="/">
          <Button onClick={deleteWedding} color="primary">
            delete
          </Button>
          </Link>
        </DialogActions>
      </Dialog>
    </>
    )
  }

  function EditExpenseDialog(){
    return(
      <>
      <Dialog open={openEditExpenseDialog} onClose={handleCloseEditExpenseDialog} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Edit Expense</DialogTitle>
        <DialogContent>
          <DialogContentText>
          Make changes to the expense details and then press the submit button to commit the changes.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="amount"
            type="currency"
            inputRef={amountInput}
            placeholder={new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(currentWeddingState.expense.amount)}
            fullWidth
          />
          <TextField
            multiline
            margin="dense"
            id="multilane"
            label="reason"
            type="text"
            inputRef={reasonInput}
            placeholder={currentWeddingState.expense.reason}
            fullWidth
          />
           <TextField
            // margin="dense"
            // id="outlined-size-normal"
            label="receiptImg"
            type="file"
            fullWidth
            onChange={getFile}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseEditExpenseDialog} color="primary">
            Cancel
          </Button>
          <Button onClick={editExpense} color="primary">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
      </>
    );
  }

  function AddExpenseDialog(){
    return(
    <>
    <Dialog open={openAddExpense} onClose={handleCloseAddExpense} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add Expense</DialogTitle>
        <DialogContent>
          {/* <DialogContentText>
          </DialogContentText> */}
          <TextField
            margin="dense"
            id="name"
            label="amount"
            type="currency"
            inputRef={amountInput}
            fullWidth
          />
          <TextField
            multiline
            margin="dense"
            id="multilane"
            label="reason"
            type="text"
            inputRef={reasonInput}
            fullWidth
          />
          <TextField
            // margin="dense"
            // id="outlined-size-normal"
            label="receiptImg"
            type="file"
            fullWidth
            onChange={getFile}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseAddExpense} color="primary">
            Cancel
          </Button>
          <Button onClick={addExpense} color="primary">
            Submit
          </Button>
        </DialogActions>
      </Dialog>

    </>);
  }

  function ViewReceiptDialog(){
    return(
      <>
      <Dialog 
      open={openViewReceipt} 
      onClose={handleCloseViewReceipt}
      className={classes.dialog}
      >
        <ViewReceiptCard receiptImg={currentWeddingState.expense.receiptImg}/>
      </Dialog>
  </>
    )
  }

  function SuccessAlert(){
    return(
      <>
        <Snackbar open={openSuccessAlert} autoHideDuration={6000} onClose={handleCloseSuccessAlert}>
        <Alert onClose={handleCloseSuccessAlert} severity="success">
            Success!
        </Alert>
        </Snackbar>
    </>
    );
  }

  return (
  <>
  <h1 className={classes.title}>Wedding</h1>
  <WeddingContainer/>
  <EditWeddingDialog/>
  <EditExpenseDialog/>
  <AddExpenseDialog/>
  <DeleteWeddingConfirmation/>
  <SuccessAlert/>
  <ViewReceiptDialog/>
  <h1 className={classes.title}>Expenses</h1>
  {expenseCards}
  </>
  );
}



