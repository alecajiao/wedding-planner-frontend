import { createStyles, Theme, makeStyles, alpha } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import WeddingCard from './wedding-card';

// const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);

export default function DisplayWeddings() {
  const classes = useStyles();

  return (<main className={classes.content}>
    <Toolbar />
      <WeddingCard></WeddingCard>
  </main>);
}
