import { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { WeddingAPI } from '../apis/wedding-api';
import Grid from '@material-ui/core/Grid';
import CardActionArea from '@material-ui/core/CardActionArea';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux"

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
});


const getWeddings = async () => {
    return await WeddingAPI.getWeddings();
}

export default function WeddingCard() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [weddings, setWeddings] = useState([] as any[]);

  useEffect(() => {
      getWeddings().then((data) => {setWeddings(data || [] as any[])});
  }, []);

  const storeCurrentWedding = (w:any)=>{
    console.log(w);
    dispatch({type:"saveWedding",wedding: w });  
  };

  const weddingsCards = weddings.slice(0).reverse().map((w:any) => {
      return(<Grid item  xs={4} key={w.id}>
        <Link to="/wedding">
        <Card className={classes.root} variant="outlined" elevation={3} onClick={()=>{storeCurrentWedding(w)}}>
          <CardActionArea>
            <CardContent>
              <Typography variant="h5" component="h2">
                {`${w.client_fname} ${w.client_lname}`}
              </Typography>
              <Typography variant="body2" component="p">
                {new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(w.budget)}
              </Typography>
              <Typography variant="body2" component="p">
                {w.location}
              </Typography>
              <Typography variant="body2" component="p" color="textSecondary">
                {(new Date(w.due)).toDateString()}
              </Typography>
            </CardContent>
          {/* <CardActions>
            <Button size="small">Learn More</Button>
          </CardActions> */}
          </CardActionArea>
        </Card>
        </Link> 
      </Grid>)
  }
  );
    
  return (<Grid
    container
    direction="row"
    justifyContent="flex-start"
    alignItems="flex-start"
    spacing={3}
  >
    {weddingsCards}
  </Grid>)
}


