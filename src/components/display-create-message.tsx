import Grid from '@material-ui/core/Grid';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import CreateMessage from './create-message';
import CreateWedding from './create-wedding';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);

export default function DisplayCreateMessage(){
    const classes = useStyles();

    return(
    <main className={classes.content}>
        <Toolbar />
        <CreateMessage/>
    </main>);
}